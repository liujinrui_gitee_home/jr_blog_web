import request from '@/utils/request'
export const addCategory = (body) => {
  return request({
    method: 'post',
    url: 'category/add',
    data: body
  })
}
export const categoryPage = (current,size,param) => {
  return request({
    method: 'get',
    url: 'category/page',
    params: {current:current,size:size,name:param.name,startTime:param.startTime,endTime:param.endTime}
  })
}
export const categoryUpdate = (id,name)=> {
  return request({
    method: 'put',
    url: 'category/update',
    data: {id:id,name:name}
  })
}
export const categoryDelete = (id)=> {
  return request({
    method: 'delete',
    url: `category/deleteById/${id}`
  })
}

