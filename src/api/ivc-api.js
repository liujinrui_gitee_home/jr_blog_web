import request from '@/utils/request'
export const getIvc = () => {
  return request({
    method: 'get',
    url: 'ivc/get',
    responseType: "blob"
  })
}
