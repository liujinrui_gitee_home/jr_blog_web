import request from '@/utils/request'
//StuCourseRecord
export const addStuCourseRecord = (body) => {
  return request({
    method: 'post',
    url: 'stuCourseRecord/add',
    data: body
  })
}

export const deleteStuCourseRecord = (id) => {
  return request({
    method: 'delete',
    url: `stuCourseRecord/deleteById/${id}`
  })
}

export const updateStuCourseRecord = (body) => {
  return request({
    method: 'put',
    url: 'stuCourseRecord/update',
    data: body
  })
}

export const getByIdStuCourseRecord = (id) => {
  return request({
    method: 'get',
    url: `stuCourseRecord/getById/${id}`
  })
}

export const getAllNames = () => {
  return request({
    method: 'get',
    url: 'stuCourseRecord/getAllNames'
  })
}

export const pageStuCourseRecord = (current,size,keyWords,name) => {
  return request({
    method: 'get',
    url: 'stuCourseRecord/page',
    params:{current:current,size:size,keyWords:keyWords,name:name}
  })
}

export const deleteBatchIdsCourseRecord = (ids) => {
  return request({
    method: 'delete',
    url: 'stuCourseRecord/deleteBatchIds',
    data:ids
  })
}
