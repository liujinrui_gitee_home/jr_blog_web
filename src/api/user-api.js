import request from '@/utils/request'
import {useUserStore} from "@/stores/user.js";
const userStore = useUserStore();
export const register = (username, password,ivcId,ivcCode) => {
  return request({
    method: 'post',
    url: 'user/register',
    data: {
      username: username,
      password: password,
      ivcId: ivcId,
      ivcCode: ivcCode
    }
  })
}

export const login = (username, password, timeStamp,ivcId,ivcCode) => {
  return request({
    method: 'post',
    url: 'user/login',
    data: {
      username: username,
      password: password,
      timeStamp: timeStamp,
      ivcId: ivcId,
      ivcCode: ivcCode
    }
  })
}

export const updatePassword = (obj) => {
  return request({
    method: 'put',
    url: 'user/updatePassword',
    data: obj
  })
}

export const getUserInfoByToken = () => {
  return request({
    method: 'get',
    url: 'user/getUserInfoByToken'
  })
}
