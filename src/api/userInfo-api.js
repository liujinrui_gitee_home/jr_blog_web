import request from '@/utils/request'
export const updateByToken = (body) => {
  return request({
    method: 'put',
    url: 'userInfo/updateByToken',
    data: body
  })
}
