import { createRouter, createWebHistory } from 'vue-router'
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {path:'/',redirect:'/login' },
    {path:'/login',component: () => import('@/views/login/login.vue') },
    {path:'/layout',component: () => import('@/views/layout/layout.vue'),redirect:'/articleManage',
    children:[
        {path:'/articleManage',component: () => import('@/views/article/articleManage.vue')},
        {path:'/categoryManage',component: () => import('@/views/article/categoryManage.vue')},
        {path:'/stuCourseRecord',component: () => import('@/views/stuCourseRecord/stuCourseRecord.vue')},
        {path:'/userInfo',component: () => import('@/views/user/userInfo.vue')}
    ]},
    {path:'/*',component: () => import('@/views/404/pageNotFound.vue')}
  ]
})

export default router
