import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useUserStore = defineStore('user', () => {
  const token = ref('')
  const remember = ref(false)
  function setRemember(value) {
    remember.value = value
  }
  function setToken(inputToken) {
    token.value = inputToken
  }
  function removeToken() {
    token.value = ''
  }
  return { token,setToken,removeToken,remember,setRemember }
},{
  persist: true
})
