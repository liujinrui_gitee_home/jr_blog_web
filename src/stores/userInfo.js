import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useUserInfoStore = defineStore('userInfo', () => {
  const userInfo = ref({
    id:0,
    userId:0,
    username:'',
    nickName:'',
    motto:'',
    birthday:'',
    email:'',
    phoneNum:'',
    defFlag:0,
    createTime:'',
    updateTime:'',
  })
  function set(obj) {
    userInfo.value.userId = obj.id
    userInfo.value.username = obj.username
    userInfo.value.nickName = obj.userInfo.nickName
    userInfo.value.motto = obj.userInfo.motto
    userInfo.value.birthday = obj.userInfo.birthday
    userInfo.value.email = obj.userInfo.email
    userInfo.value.phoneNum = obj.userInfo.phoneNum
    userInfo.value.createTime = obj.createTime
  }
  return { userInfo,set }
},{
  persist: false
})
