import axios from "axios";
import {useUserStore} from "@/stores/user.js";
import router from "@/main.js";
const userStore = useUserStore()
const request = axios.create({
    baseURL: 'http://127.0.0.1:8081/',
    timeout: 10000
})
// 添加请求拦截器
request.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    config.headers.set('USER-TOKEN',userStore.token)
    return config
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error)
})

// 添加响应拦截器
request.interceptors.response.use(function (response) {
    // 2xx 范围内的状态码都会触发该函数。
    // 对响应数据做点什么
    if(response.data.code === 401){

        ElNotification({
            title: 'Error',
            message: '用户凭证已过期,请重新登录!',
            type: 'error',
        })
        userStore.removeToken()
        userStore.setRemember(false)
        router.replace('/login')
        return response
    }
    return response
}, function (error) {
    // 超出 2xx 范围的状态码都会触发该函数。
    // 对响应错误做点什么
    return Promise.reject(error)
})
export default request
